Rails.application.routes.draw do
  devise_for :admins
  resources :test_interventions
  resources :agendas
  devise_for :users
  get 'users/', to: 'users#index'
  get 'test_interventions/', to: 'test_interventions#show'

  root to: "application#index"
end
