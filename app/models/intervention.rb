class Intervention
  include Mongoid::Document
  include Mongoid::Enum

  field :address, type: String
  field :bat, type: String
  field :photo, type: String
  enum :orientation, [:nord, :sud, :est, :ouest, :'nord_est', :'nord_ouest', :'sud_est', :'sud_ouest',
                      :'nord_nord_est', :'nord_est_est', :'nord_nord_ouest', :'nord_ouest_ouest', :'sud_sud_est',
                      :'sud_est_est', :'sud_sud_ouest', :'sud_ouest_ouest']
  enum :state, [:'en_attente', :'en_cours', :'finis', :'refusé'], default: 'en attente'

# for refused
  field :raison, type: String
# for accepted
  field :startdate, type: DateTime
  field :enddate, type: DateTime
end
