class Agenda
  include Mongoid::Document

  field :start, type: DateTime
  field :end, type: DateTime

  belongs_to :test_intervention

end
