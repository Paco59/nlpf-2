class TestIntervention
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  field :title, type: String
  field :image_data, type: String
  include ImageUploader::Attachment.new(:image)

  field :address, type: String
  field :bat, type: String
  field :photo, type: String
  field :orientation, type: String 
  field :state, type: String, default:'en attente'
  field :raison, type: String
  belongs_to :user
  has_one :agenda, autobuild: true, autosave: true
end
