json.extract! test_intervention, :id, :address, :bat, :photo, :orientation, :state, :raison, :startdate, :endate, :created_at, :updated_at
json.url test_intervention_url(test_intervention, format: :json)
