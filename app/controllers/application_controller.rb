class ApplicationController < ActionController::Base
  layout "application"

def index
  render template: 'index.html.erb'
end

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:nom, :prenom, :email, :password)}

    devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:nom, :prenom, :email, :password, :current_password, :blacklisted)}
 end
end
