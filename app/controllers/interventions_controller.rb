class InterventionsController < ApplicationController
  def add
  end

  def finish
  end

  def accept
  end

  def refuse
  end

  def show
    render template: "interventions/show.html.erb"
  end
end
