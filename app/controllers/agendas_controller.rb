class AgendasController < ApplicationController
#  before_action :set_agenda, only: [:show, :edit, :update, :destroy]

  # GET /agendas
  # GET /agendas.json
  def index
    @meetings = TestIntervention.all
    @agendas = []
    @rela = Hash.new

    @meetings.each do |m|
      a = m.agenda
      @agendas.push(a)
      @rela[a[:test_intervention_id]] = {:address => m.address, :bat => m.bat, :orientation => m.orientation}
        # @agendas.push({:start_time => m.agenda.start, :end =>  m.agenda.end, :address => m.address, :bat => m.bat, :orientation => m.orientation})
    end
    puts(@agendas.inspect)
    puts(@rela.inspect)
  end

  # GET /agendas/1
  # GET /agendas/1.json
  def show
  end

  # GET /agendas/new
  def new
    @agenda = Agenda.new
  end

  def self.next_agenda
    firstAgenda = Agenda.first_or_initialize

    if  firstAgenda[:start] == nil
      now = DateTime.now
      now = now.change({hour: now.hour+1})
    else
     now = Agenda.last[:end]
    end
    nextAgenda = Agenda.new
    puts("Retrieve Agenda")
    puts(now.inspect)
    if (now.hour < 9)
      puts("Too soon", now.inspect)
      nextAgenda[:start] = now.change({hour: 9, min: 0})
      nextAgenda[:end] = now.change({hour: 10, min: 0})
    elsif (now.hour >= 18)
      puts("Too late", now.inspect)
      nextAgenda[:start] = now.tomorrow.change({hour: 9, min: 0})
      nextAgenda[:end] = now.tomorrow.change({hour: 10, min: 0})
    else
      puts("Correct time")
      nextAgenda[:start] = now.change({hour: now.hour, min: 0})
      nextAgenda[:end] = now.change({hour: now.hour + 1, min: 0})
    end
    puts("next_agenda")
    puts(nextAgenda.inspect)
    return nextAgenda
  end

  def last
    @agendas = Agenda.last
    render template: 'agendas/index'
  end

  # GET /agendas/1/edit
  def edit
  end

  # POST /agendas
  # POST /agendas.json
  def create
    @agenda = Agenda.new(agenda_params)

    respond_to do |format|
      if @agenda.save
        format.html { redirect_to @agenda, notice: 'Agenda was successfully created.' }
        format.json { render :show, status: :created, location: @agenda }
      else
        format.html { render :new }
        format.json { render json: @agenda.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /agendas/1
  # PATCH/PUT /agendas/1.json
  def update
    respond_to do |format|
      if @agenda.update(agenda_params)
        format.html { redirect_to @agenda, notice: 'Agenda was successfully updated.' }
        format.json { render :show, status: :ok, location: @agenda }
      else
        format.html { render :edit }
        format.json { render json: @agenda.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /agendas/1
  # DELETE /agendas/1.json
  def destroy
    @agenda.destroy
    respond_to do |format|
      format.html { redirect_to agendas_url, notice: 'Agenda was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agenda
      @agenda = Agenda.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agenda_params
      params.fetch(:agenda, {})
    end
end
