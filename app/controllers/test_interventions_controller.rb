class TestInterventionsController < ApplicationController
  before_action :set_test_intervention, only: [:show, :edit, :update, :destroy]

  # GET /test_interventions
  # GET /test_interventions.json
  def index
    if admin_signed_in?
      @test_interventions = TestIntervention.all
    else
    @test_interventions = TestIntervention.where(:user => current_user)
      end
  end

  # GET /test_interventions/1
  # GET /test_interventions/1.json
  def show
    @interventions = TestIntervention.all
  end

  # GET /test_interventions/new
  def new
    @test_intervention = TestIntervention.new
  end

  # GET /test_interventions/1/edit
  def edit
  end

  # POST /test_interventions
  # POST /test_interventions.json
  def create
    @test_intervention = TestIntervention.new(test_intervention_params)

    @test_intervention.user = current_user
    respond_to do |format|
      if @test_intervention.save
        format.html { redirect_to @test_intervention, notice: 'Test intervention was successfully created.' }
        format.json { render :show, status: :created, location: @test_intervention }
      else
        format.html { render :new }
        format.json { render json: @test_intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_interventions/1
  # PATCH/PUT /test_interventions/1.json
  def update
    respond_to do |format|
      if test_intervention_params[:state] == "accepter" && @test_intervention.state != "accepter"
        @test_intervention.agenda = AgendasController.next_agenda
      end
      if @test_intervention.update(test_intervention_params)
        format.html { redirect_to @test_intervention, notice: 'Test intervention was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_intervention }
      else
        format.html { render :edit }
        format.json { render json: @test_intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_interventions/1
  # DELETE /test_interventions/1.json
  def destroy
    @test_intervention.destroy
    respond_to do |format|
      format.html { redirect_to test_interventions_url, notice: 'Test intervention was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_intervention
      @test_intervention = TestIntervention.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_intervention_params
      params.require(:test_intervention).permit(:address, :bat, :image, :orientation, :state, :raison, :startdate, :endate)
    end
end
