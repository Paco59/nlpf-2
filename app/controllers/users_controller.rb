class UsersController < ApplicationController
  def show
    @user = User.all
  end

  def index
    @users = User.all
  end

  def block
    @user.blacklisted = !@user.blacklisted
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'user was successfully updated.' }
        format.json { render :show, status: :ok, location: @user}
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

end
